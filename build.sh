#!/usr/bin/env bash
# arg1: name of image
# arg2: wordpress version
# arg3: php version

SOURCE=$1
IMAGE_NAME=$2
WORDPRESS_VERSION=${3:-5.2.2}
PHP_VERSION=${4:-7.2.15}
SCRIPT_DIR=$(pwd)

tag_name="$IMAGE_NAME:$WORDPRESS_VERSION-php$PHP_VERSION-$SOURCE" 

cd $SOURCE

docker build \
  --tag $tag_name \
  --build-arg WORDPRESS_VERSION=$WORDPRESS_VERSION \
  --build-arg PHP_VERSION=$PHP_VERSION \
  .

docker push $tag_name

cd $SCRIPT_DIR

#!/bin/bash
cat >/etc/motd <<EOL
       __  __ _       _
      |  \\/  (_)     | |
   ___| \\  / |_  ___ | | ___
  / _ \\ |\\/| | |/ _ \\| |/ _ \\
 |  __/ |  | | | (_) | | (_) |
  \\___|_|  |_|_|\\___/|_|\\___/

W O R D P R E S   O N   L I N U X

PHP version : $PHP_VERSION
WORDPRESS version : $WORDPRESS_VERSION
EOL
cat /etc/motd

# Get environment variables to show up in SSH session
eval $(printenv | sed -n "s/^\([^=]\+\)=\(.*\)$/export \1=\2/p" | sed 's/"/\\\"/g' | sed '/=/s//="/' | sed 's/$/"/' >> /etc/profile)

sed -i "s/SSH_PORT/$SSH_PORT/g" /etc/ssh/sshd_config
sed -i "s@{PORT}@$PORT@g" /etc/nginx/nginx.conf
sed -i "s@{WEBAPP_DIR}@$WEBAPP_DIR@g" /etc/nginx/nginx.conf

echo "Configuring Wordpress"
cd /var/www/html
if [ ! -e wp-config.php ]; then
  wp config create --allow-root \
    --dbhost="${WORDPRESS_DB_HOST:=mysql}" \
    --dbuser="${WORDPRESS_DB_USER:=root}" \
    --dbpass="${WORDPRESS_DB_PASSWORD:=}" \
    --dbname="${WORDPRESS_DB_NAME:=wordpress}" \
    --dbcharset="${WORDPRESS_DB_CHARSET:=utf8}" \
    --dbcollate="${WORDPRESS_DB_COLLATE:=}" \
    --skip-check
  envs=(
    WP_DEBUG
    WP_DEBUG_LOGS
  )
  for key in "${envs[@]}"; do
    value="${!key}"
    if [ -n "$value" ]; then
      wp config set --allow-root "$key" "$value" --raw
    fi
  done
fi

echo "Starting SSH ..."
service ssh start

echo "Starting php-fpm ..."
php-fpm -e

echo "Starting Nginx ..."
mkdir -p /home/LogFiles/nginx
if test ! -e /home/LogFiles/nginx/error.log; then
    touch /home/LogFiles/nginx/error.log
fi
/usr/sbin/nginx -g "daemon off;"

#!/bin/bash

VERSION="$1"

echo === Installing PHP Extensions

if [[ "$VERSION" < "7" ]]; then
  echo == Installing old MySQL driver
  docker-php-ext-install mysql
fi

if [[ ( "$VERSION" > "7" ) || ( "$VERSION" = "7" ) ]]; then
  echo == Installing new MySQL drivers
  docker-php-ext-install pdo pdo_mysql
fi

#!/bin/bash

TAG=$(date -u +"%y%m%d%H%M")
INSTALL_DIR=$(pwd)
TMP_DIR="/tmp/wp-${TAG}"
VERSION=$1

echo === Install Wordpress $VERSION ===
echo Install dir: $INSTALL_DIR
echo

echo == Check if directory is empty

shopt -s nullglob
shopt -s dotglob

chk_files=(${INSTALL_DIR}/*)
if (( ${#chk_files[*]} )); then
  echo Install directory is not empty.
  echo
  read -p "Do you really want to continue?" -n 1 -r
  echo    # (optional) move to a new line
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
  fi
fi

shopt -u nullglob
shopt -u dotglob

echo == Create temporary dir
mkdir -p $TMP_DIR
cd $TMP_DIR

echo == Download Wordpress $VERSION
wget "https://wordpress.org/wordpress-$VERSION.zip" -O wordpress.zip

echo == Unzip files
unzip wordpress.zip

echo == Move files
mv -f wordpress/* $INSTALL_DIR
mv -f wordpress/.* $INSTALL_DIR

echo == Clean temporary files
cd $INSTALL_DIR
rm -rf $TMP_DIR

echo === Done
